
function expandCollapse(index) {
    var icon = document.getElementById(`icon-${index}`);
    var desc = document.getElementById(`expand-ps-${index}`);

    if (icon.className.includes("chevron-right")) {
        desc.removeAttribute("hidden");
        icon.className = "fas fa-chevron-down";
    } else {
        desc.setAttribute("hidden", "");
        icon.className = "fas fa-chevron-right";
    }
}
